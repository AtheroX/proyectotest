# Runner - Lucas Ruiz

Risketos Básics:
 - [x] Controls simples, ja sigui clicar un o dos botons.
 - [ ] Moviment amb collisions.
 - [ ] Aleatorietat.
 - [x] Creació d’objectes per Blueprint, ja sigui d’inici o un spawner (Relacionat amb el 1r Risketo Opcional).
 - [ ] Puntuació mostrada per HUD, no val debug.

Risketos Opcionals:
- [x] Augment progressiu de dificultat
- [ ] Ús de parallax o altre format avançat de background
- [ ] Guardar y cargar partida

Controles:
- Espacio o click izquierdo para saltar
- A y D para moverse horizontalmente.

